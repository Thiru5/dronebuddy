import cv2
import numpy as np

forwardBackwardRange = [6200, 6800]


def findFace(img):
    faceCascade = cv2.CascadeClassifier("../data/haarcascades/haarcascade_frontalface_default.xml")
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # returns a list of squares and change values to get better detection
    faces = faceCascade.detectMultiScale(imgGray, 1.2, 8)

    myFaceListCenter = []
    myFaceListArea = []

    for (x, y, w, h) in faces:
        # draw rectangle
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
        centerX = x+w // 2
        centerY = y+h // 2
        area = w * h

        #getting center value and displaying it as circle.
        cv2.circle(img, (centerX, centerY), 5, (0, 255, 0), cv2.FILLED)
        myFaceListCenter.append([centerX, centerY])
        myFaceListArea.append(area)

        #find the max area and send that value back

        if len(myFaceListArea) != 0:
            i = myFaceListArea.index(max(myFaceListArea))
            #pass the centerX and centerY values
            return img, [myFaceListCenter[i], myFaceListArea[i]]
        else:
            return img, [[0, 0], 0]

    return img, [[0, 0], 0]


def trackFace(droneBuddy, information, w, pid, pError):

    area = information[1]
    x,y = information[0]
    forwardBackwardSpeed = 0

    #how far away from center
    error = x - w // 2

    #cahnge sensitivity of error with this value
    speed = pid[0]*error + pid[1]*(error - pError)

    #changing yaw
    speed = int(np.clip(speed, -100, 100))

    #when stationary
    if area > forwardBackwardRange[0] and area < forwardBackwardRange[1]:
        forwardBackwardSpeed = 0
    #when too close
    elif area > forwardBackwardRange[1]:
        forwardBackwardSpeed = -30
    #when too far
    elif area < forwardBackwardRange[0] and area != 0:
        forwardBackwardSpeed = 20

    if x == 0:
        speed = 0
        error = 0


    droneBuddy.send_rc_control(0,forwardBackwardSpeed,0, speed)
    return error

