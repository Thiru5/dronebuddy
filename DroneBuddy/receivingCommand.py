import pyrebase
from faceTrackingInitialize import *
from connectDroneBuddy import *
from moveCommand import *
from networkPrioritySwitching import *

switchFromEthernetToTello()
droneBuddy = connect()
switchFromTelloToEthernet()

config = {
    "apiKey": "AIzaSyCYkRRsyxstHbP-J1sdNwdAG3AO5fYYpYw",
    "authDomain": "dronebuddy",
    "databaseURL": "https://dronebuddy-default-rtdb.asia-southeast1.firebasedatabase.app/",
    "storageBucket": "dronebuddy.appspot.com"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()




def stream_handler1(message):
    print(message["event"])  # put
    print(message["path"])  # /-K7yGTTEp7O549EzTYtI
    print(message["data"])  # {'title': 'Pyrebase', "body": "etc..."}

    data = message["data"]

    if "-" in data:
        d = data.split("-")
        command = d[0]
        distance = int(d[1])
        if command != 'facetrack':
            print("Drone buddy moving " + command + " by " + str(distance))
            switchFromEthernetToTello()
            moveCommand(droneBuddy, command, distance)
            switchFromTelloToEthernet()
        elif command == 'facetrack':
            print("Init FaceTracking")
            switchFromEthernetToTello()
            faceTrackingInitialize(droneBuddy)
            switchFromTelloToEthernet()


my_stream = db.child("droneCommand").stream(stream_handler1)
