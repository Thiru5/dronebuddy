import time
from djitellopy import tello
from localFunctions.faceTracking import *
import requests
import cv2
from networkPrioritySwitching import *


FINDFACE_URL = 'https://asia-southeast1-dronebuddy.cloudfunctions.net/findFace'
TRACKFACE_URL = 'https://asia-southeast1-dronebuddy.cloudfunctions.net/facetrack'

def faceTrackingInitialize(droneBuddy):
    pid = [0.4, 0.4, 0]
    pError = 0
    w, h = 360, 240

    switchFromEthernetToTello()

    droneBuddy.streamon()
    droneBuddy.takeoff()
    droneBuddy.send_rc_control(0, 0, 25, 0)
    time.sleep(1.6)

    # choosing camera for video capture
    # capture = cv2.VideoCapture(0)

    while True:
        # _, img = capture.read()
        img = droneBuddy.get_frame_read().frame
        img = cv2.resize(img, (w, h))
        faceParams = {'img': img}
        switchFromTelloToEthernet()
        img, information = requests.post(FINDFACE_URL, json=faceParams)
        trackParams = {'droneBuddy': droneBuddy, 'information': information, 'w': w, 'pid': pid, 'pError': pError}
        pError, forwardBackwardSpeed, speed = requests.post(TRACKFACE_URL, json=trackParams)
        # print("Area:", information[1], "Center: ", information[0])
        switchFromEthernetToTello()
        droneBuddy.send_rc_control(0, forwardBackwardSpeed, 0, speed)
        cv2.imshow("Output", img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            droneBuddy.land()
            break

# the drone will move according to increase or decrease of area and also position of the center point
