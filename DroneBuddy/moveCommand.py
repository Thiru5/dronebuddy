from djitellopy import tello
from networkPrioritySwitching import *


def moveCommand(dronebuddy, command, distance):

    if command == 'takeoff':
        dronebuddy.takeoff()
    elif command == 'land':
        dronebuddy.land()
    elif command == 'forward':
        dronebuddy.move_forward(distance)
    elif command == 'backward':
        dronebuddy.move_backward(distance)
    elif command == 'left':
        dronebuddy.move_left(distance)
    elif command == 'right':
        dronebuddy.move_right(distance)
    elif command == 'up':
        dronebuddy.move_up(distance)
    elif command == 'down':
        dronebuddy.move_down(distance)