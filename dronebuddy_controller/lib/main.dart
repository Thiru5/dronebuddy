import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:math';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Buddy Controller',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder(
        future: _fbApp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print('Firebase did not initialize properly ${snapshot.error.toString()}');
            return Text("Something went wrong");
          } else if (snapshot.hasData) {
              return MyHomePage(title: "My Drone Buddy Controller");
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      )
      //const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  DatabaseReference ref = FirebaseDatabase.instance.ref('droneCommand');

  void _incrementCounter() {

    ref.set("Hello world ${Random().nextInt(100)}");
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  void _sendCommand(message, distance){
    message = message + '-' + distance.toString();
    ref.child('command').set(message);
    print("Drone action: $message by $distance");
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        children:<Widget>[
          MyStatelessWidget(),
        ]
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class MyStatelessWidget extends StatelessWidget{
  const MyStatelessWidget({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        OutlinedButton(

          onPressed: (){
            _MyHomePageState()._sendCommand("takeoff", 0);
          },
          child: const Text('Drone taking off'),

        ),
        OutlinedButton(

          onPressed: (){
            _MyHomePageState()._sendCommand("land", 0);
          },
          child: const Text('Drone landing'),

        ),
        OutlinedButton(

          onPressed: (){
            _MyHomePageState()._sendCommand("up", 10);;
          },
          child: const Text('Drone move up'),

        ),
        OutlinedButton(
          onPressed: (){
            _MyHomePageState()._sendCommand("down", 10);
          },
          child: const Text('Drone move down'),

        ),
        OutlinedButton(
          onPressed: (){
            _MyHomePageState()._sendCommand("forward", 10);
          },
          child: const Text('Drone move forward'),

        ),
        OutlinedButton(
          onPressed: (){
            _MyHomePageState()._sendCommand("backward", 10);
          },
          child: const Text('Drone move back'),

        ),
        OutlinedButton(
          onPressed: (){
            _MyHomePageState()._sendCommand("facetrack", 0);
          },
          child: const Text('Drone track face'),

        ),
      ]
    );

  }
}


